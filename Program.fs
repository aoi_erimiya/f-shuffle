﻿(*
  Program.fs

  Copyright (c) 2018 Hiroaki Wada

  This software is released under the MIT License.
  http://opensource.org/licenses/mit-license.php
*)
open System
open System.Linq

open BenchmarkDotNet
open BenchmarkDotNet.Attributes
        
let showCards(label:string, cards:List<int>) =
    printf "%s -> " label
    cards |> List.iter (fun card -> printf "%d," card)
    printfn ""

[<AbstractClass; Sealed>]
type Math private() =
    static let rand = Random();
    static member Next(arg : int) : int =
        rand.Next(arg)

type CardNumber<[<EqualityConditionalOn; ComparisonConditionalOn >]'T>(value : 'T) =
    member x.Value = value

    override x.Equals(comparisonObject) = 
        match comparisonObject with
        | :? CardNumber<'T> as y -> Unchecked.equals x.Value y.Value
        | _ -> false

    override x.GetHashCode () =
        Unchecked.hash x.Value

    interface System.IComparable with
        member x.CompareTo _ = 
            match Math.Next(2) = 0 with
            | true -> -1
            | _ -> 1
    
    override x.ToString () =
        x.Value.ToString()

type Benchmarks() = 
    [<Benchmark>]
    member this.patternShuffled() = 
        let rand = Random()

        let mutable cards = [1;2;3;4;5;6;7]
        while cards.Length < 28 do
            let randCard = rand.Next(1, 8)
            List.countBy (fun elem -> if (elem = randCard) then 1 else 0) cards
            |> List.filter (fun tpl -> fst(tpl) = 1 && snd(tpl) < randCard)
            |> List.iter (fun x -> cards <- List.append [randCard] cards)
            |> ignore

        //showCards("Shuffled", cards) |> ignore

    [<Benchmark>]
    member this.patternLinq() = 
        let mutable cards = []
        for i in 1..7 do
            for _ in 1..i do
                cards <- List.append [i] cards

        let mutable shuffledCards = []
        let shuffledGenericCards: ResizeArray<int32> = cards.OrderBy(fun _ -> Guid.NewGuid()).ToList()
        shuffledGenericCards.ForEach(fun x -> shuffledCards <- List.append [x] shuffledCards)

        //showCards("Linq    ", shuffledCards) |> ignore

    [<Benchmark>]
    member this.patternListSwap() = 
        let rand = Random()

        let mutable cards = []
        for i in 1..7 do
            for _ in 1..i do
                cards <- List.append [i] cards

        let mutable swapCards = cards
        let cardsLength = cards.Length-1
        for i in 0..cardsLength  do
            let targetIdx = rand.Next(0, cards.Length)
            if not (targetIdx = i) then
                if i < targetIdx then
                    swapCards <- List.append(List.append (List.append (List.append swapCards.[0..i-1] swapCards.[targetIdx..targetIdx]) swapCards.[i+1..targetIdx-1]) swapCards.[i..i]) swapCards.[targetIdx+1..cardsLength]
                else
                    swapCards <- List.append(List.append (List.append (List.append swapCards.[0..targetIdx-1] swapCards.[i..i]) swapCards.[targetIdx+1..i-1]) swapCards.[targetIdx..targetIdx]) swapCards.[i+1..cardsLength]

        //showCards("Swap    ", swapCards) |> ignore

    [<Benchmark>]
    member this.patternBucket() = 
        let rand = Random()

        let mutable cards = []
        for i in 1..7 do
            for _ in 1..i do
                cards <- List.append [i] cards

        let mutable idxBucket = []
        let cardsLength = cards.Length
        let rec loop() = 
            if idxBucket.Length = cardsLength then
                ()
            else
                let targetIdx = rand.Next(0, cardsLength)
                let isFindIdx = List.exists (fun idx -> idx = targetIdx) idxBucket
                if not isFindIdx then    
                    idxBucket <- List.append [targetIdx] idxBucket
                loop()
        loop()

        let mutable bucketSortCards = []
        for i in 0..cardsLength-1 do
            bucketSortCards <- List.append [cards.Item(idxBucket.Item(i))] bucketSortCards

        //showCards("Bucket  ", bucketSortCards) |> ignore

    [<Benchmark>]
    member this.patternArraySwap() = 
        let rand = Random();
        let mutable cards = []
        for i in 1..7 do
            for _ in 1..i do
                cards <- List.append [i] cards

        let swap (array: _[]) x y =
            let tmp = array.[x]
            array.[x] <- array.[y]
            array.[y] <- tmp

        // shuffle an array (in-place)
        let shuffle array =
            Array.iteri (fun i _ -> swap array i (rand.Next(i, Array.length array))) array

        let cardArray = List.toArray cards
        shuffle cardArray
        //showCards("Array  ", Array.toList cardArray) |> ignore

    [<Benchmark>]
    member this.patternIComparable()  = 
        let mutable cards = []
        for i in 1..7 do
            for _ in 1..i do
                cards <- List.append [(CardNumber(i))] cards
    
        //let mutable shuffledCards = List.sort cards
        //shuffledCards |> List.iter (fun card -> printf "%d," card.Value)
        //printfn ""

    [<Benchmark>]
    member this.patternRangeCutSortToList() = 
        let arrayLength = 28
        let cards = Array.create arrayLength 0
        for i in 1..7 do
            for j in 0..i do
                if j < i then
                    cards.[i * (i-1)/2 + j] <- i
        
        let shuffledCards = Array.create arrayLength 0
        for i in 0..arrayLength - 1 do
            let rand = Math.Next(arrayLength - i)
            shuffledCards.[i] <- cards.[rand]
            cards.[rand] <- cards.[arrayLength - i - 1]
        
        //ver.Array
        //shuffledCards |> Array.iter (fun card -> printf "%d," card)
        //printfn ""
        
        //ver.List
        //showCards("Array  ", Array.toList shuffledCards) |> ignore
        

[<EntryPoint>]
let main argv = 
    let _summary = Running.BenchmarkRunner.Run<Benchmarks>()
    
    0 // return an integer exit code
